package com.dev.artemt.productservice;

import com.dev.artemt.productservice.dto.ProductRequest;
import com.dev.artemt.productservice.dto.ProductResponse;
import com.dev.artemt.productservice.model.Product;
import com.dev.artemt.productservice.repository.ProductRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.math.BigDecimal;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
class ProductServiceApplicationTests {

	@Container
	static MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:7.0.0-rc4");
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	ProductRepository productRepository;

	@DynamicPropertySource
	static void setProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
		dynamicPropertyRegistry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
	}

	@Test
	void shouldCreateProduct() throws Exception {
		ProductRequest productRequest = getProductRequest();
		String productRequestString = objectMapper.writeValueAsString(productRequest);

		mockMvc.perform(post("/api/products")
				.content(productRequestString)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
		Assertions.assertTrue(productRepository.findAll().size() == 1);
		productRepository.deleteAll();
	}

	@Test
	void shouldGetAllProducts() throws Exception {
		List<Product> products = List.of(
				new Product("fedr", "GRFED", "RGFE", BigDecimal.valueOf(11)),
				new Product("fed11r", "GRFED", "RGFE", BigDecimal.valueOf(12)),
				new Product("feewrftwedr", "GRferwfFED", "RGfewFE", BigDecimal.valueOf(13))
		);
		productRepository.saveAll(products);

		String productResponseString = objectMapper.writeValueAsString(products);

		mockMvc.perform(get("/api/products"))
				.andExpect(status().isOk())
				.andExpect(content().json(productResponseString));
		productRepository.deleteAll();
	}

	@Test
	void shouldGetProductById() throws Exception {
		Product product = new Product("fedr", "GRFED", "RGFE", BigDecimal.valueOf(11));
		productRepository.save(product);
		String productString = objectMapper.writeValueAsString(product);

		mockMvc.perform(get("/api/products/{id}", product.getId()))
				.andExpect(status().isOk())
				.andExpect(content().json(productString));
	}

	private ProductRequest getProductRequest() {
		return ProductRequest.builder()
				.title("Mock")
				.description("Mock product")
				.price(BigDecimal.valueOf(11))
				.build();
	}

}
