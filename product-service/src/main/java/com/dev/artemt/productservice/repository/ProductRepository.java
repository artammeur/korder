package com.dev.artemt.productservice.repository;

import com.dev.artemt.productservice.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, String> {

}
