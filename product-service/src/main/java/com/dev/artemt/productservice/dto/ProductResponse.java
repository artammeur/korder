package com.dev.artemt.productservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductResponse {
    private String id;
    //private ProductCategory category;
    private String title;
    private String description;
    private BigDecimal price;
    //private String brand;
}
