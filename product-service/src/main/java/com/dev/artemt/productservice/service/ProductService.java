package com.dev.artemt.productservice.service;

import com.dev.artemt.productservice.dto.ProductRequest;
import com.dev.artemt.productservice.dto.ProductResponse;
import com.dev.artemt.productservice.model.Product;
import com.dev.artemt.productservice.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {
    private final ProductRepository productRepository;

    public void createProduct(ProductRequest productDto) {
        Product product = Product.builder()
                        .title(productDto.getTitle())
                        .description(productDto.getDescription())
                        .price(productDto.getPrice())
                .build();

        productRepository.save(product);
        log.info("Product {} is saved", product.getId());
    }

    public ProductResponse getProductById(String id) throws HttpStatusCodeException {
        Product product = productRepository.findById(id).orElse(null);
        if (product == null) throw new HttpStatusCodeException(HttpStatus.BAD_REQUEST){} ;
        return ProductResponse.builder()
                .id(product.getId())
                .description(product.getDescription())
                .title(product.getTitle())
                .price(product.getPrice())
                .build();
    }

    public List<ProductResponse> getAllProducts() {
        List<Product> products = productRepository.findAll();
        return products.stream().map(this::mapToProductResponse).toList();
    }

    private ProductResponse mapToProductResponse(Product product) {
        return ProductResponse.builder()
                .id(product.getId())
                .description(product.getDescription())
                .title(product.getTitle())
                .price(product.getPrice())
                .build();
    }
}
