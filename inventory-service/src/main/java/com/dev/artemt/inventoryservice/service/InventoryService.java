package com.dev.artemt.inventoryservice.service;

import com.dev.artemt.inventoryservice.dto.InventoryResponse;
import com.dev.artemt.inventoryservice.repository.InventoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class InventoryService {

    private final InventoryRepository inventoryRepository;

    @Transactional(readOnly = true)
    public String isInStock(String skuCode) {
        if (inventoryRepository.findBySkuCode(skuCode).isPresent()) {
            return skuCode + "is present";
        } else {
            return "null";
        }
    }

    @Transactional(readOnly = true)
    public List<InventoryResponse> isOrderLineItemsInStock(List<String> skuCodes) {
        //simulate slow behavior
//        log.info("Wait started");
//        Thread.sleep(10000); //throw exp
//        log.info("Wait ended");
        return inventoryRepository.findBySkuCodeIn(skuCodes).stream()
                .map(inventory ->
                        InventoryResponse.builder()
                            .skuCode(inventory.getSkuCode())
                            .isInStock(inventory.getQuantity() > 0)
                            .build()
                ).toList();
    }
}
