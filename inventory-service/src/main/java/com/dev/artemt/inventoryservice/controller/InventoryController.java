package com.dev.artemt.inventoryservice.controller;

import com.dev.artemt.inventoryservice.dto.InventoryResponse;
import com.dev.artemt.inventoryservice.service.InventoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/inventory")
@RequiredArgsConstructor
public class InventoryController {
    private final InventoryService inventoryService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<InventoryResponse> isOrderLineItemsInStock(@RequestParam List<String> skuCodes) {
        return inventoryService.isOrderLineItemsInStock(skuCodes);
    }

    @GetMapping("{sku-code}")
    @ResponseStatus(HttpStatus.OK)
    public String isInStock(@PathVariable("sku-code") String skuCode) {
        return inventoryService.isInStock(skuCode);
    }
}
