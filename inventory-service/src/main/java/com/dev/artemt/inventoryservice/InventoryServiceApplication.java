package com.dev.artemt.inventoryservice;

import com.dev.artemt.inventoryservice.model.Inventory;
import com.dev.artemt.inventoryservice.repository.InventoryRepository;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@EnableDiscoveryClient
public class InventoryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventoryServiceApplication.class, args);
	}

//	@Bean
//	public CommandLineRunner loadData(InventoryRepository inventoryRepository) {
//		return args -> {
//			inventoryRepository.save(Inventory.builder()
//					.skuCode("first")
//					.quantity(11)
//					.build());
//			inventoryRepository.save(Inventory.builder()
//					.skuCode("second")
//					.quantity(22)
//					.build());
//		};
//	}
}
