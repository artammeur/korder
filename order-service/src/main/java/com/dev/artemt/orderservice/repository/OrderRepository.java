package com.dev.artemt.orderservice.repository;

import com.dev.artemt.orderservice.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository <Order, Long> {
}
